module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    'plugin:flowtype/recommended',
    'plugin:react/recommended',
    'eslint:recommended',
  ],
  globals: {
    CONFIG: true,
  },
  plugins: [
    'flowtype',
    'react',
  ],
  env: {
    browser: true,
    commonjs: true,
    es6: true,
  },
  rules: {
    'flowtype/no-types-missing-file-annotation': 'off',
    'react/prop-types': 'off',
  },
}
