# Installation

You need git and Docker.

```bash
git clone --recursive git@gitlab.com:ivan.perevezentsev/telegram-web-chat.git
cd telegram-web-chat
```

Configure the app as described in "Configuration".

```bash
docker run \
  -it \
  -v $PWD:/app \
  -p 443:443 \
  -p 9615:9615 \
  --restart always \
  --name tg-chat \
  perevezentsev/tg-chat
```

In the container's shell:

```bash
cd /app/server
yarn start:prod
```

# Configuration

All configuration resides in two places:
  - Front-end: `build_configs/definitions.js`
  - Back-end: `server/secrets/`

## Front-end

- Create a front-end configuration at `build_configs/definitions.js` in this format:

```js
const common = {
  // Firebase credentials, copy them from Firebase admin panel
  firebaseConfig: {
    apiKey: "TehGlO3giC7NA2xinEJetJRN1P1ugpuF3Up3ViW",
    authDomain: "telegram-web-chat.firebaseapp.com",
    databaseURL: "https://telegram-web-chat.firebaseio.com",
    projectId: "telegram-web-chat",
    storageBucket: "telegram-web-chat.appspot.com",
    messagingSenderId: "900532430989"
  },
  messageGroupingInterval: 1000 * 60 * 3, // In milliseconds
}

// One of these objects is inserted into front-end code via Webpack's `DefinePlugin`.

exports.productionDefinitions = {
  ...common,

  apiAddress: 'https://api.telegram-web.tk',
}

exports.developmentDefinitions = {
  ...common,

  // Here you can use the IP of your computer in your LAN to test from mobiles, i.e.:
  // apiAddress: 'http://192.168.0.14:8443',
  apiAddress: 'http://localhost:8443',
}
```

## Back-end

- Create a Firebase project
  - Copy the database rules from `database_rules.json` to https://console.firebase.google.com/project/{YOUR_PROJECT}/database/{YOUR_PROJECT}/rules

- `mkdir server/secrets && cd server/secrets`. In this directory do the following:
  - Place the HTTPS certificate and private key as `public.pem` and `private.key`
  - Download your Firebase admin credentials from Firebase Console (in project settings) and save them as `admin_credentials.json`
  - Create a server configuration like this:

```js
module.exports = {
  port: 443,
  database: {
    // Firebase DB URL
    url: 'https://telegram-web-chat.firebaseio.com',
  },
  telegram: {
    // Copy these from https://my.telegram.org
    apiId: 123456,
    apiHash: 'd920cf36bc0ac4a3b4c8005d7dd4e248',

    phoneNumber: '12345678',

    // Group chat IDs are negative, whereas person chat IDs are positive
    // Peter Green
    chatId: 92232,
    // TelegramWebChat group (dev)
    // chatId: -267663971,
    // TelegramWebChat group (prod)
    // chatId: 306494270,
  },
  firstTimeGreeting: {
    timeout: 5000,
  },
}
```

# Development

## Front-end

- Serve locally: `yarn serve`
- Deploy to production: `yarn deploy`

## Back-end

- On Linux:
  - Install the latest OpenSSL (tested with version 1.1.0)
  - Build Node.js from source. You *must* use the same OpenSSL version as used with TDLib, otherwise there will be segmentation fault at start
- Build TDLib

### How to build Docker image

Use `Dockerfile.tdlib`.

# Monitoring

You can call pm2 from the container via `yarn run pm2`.
