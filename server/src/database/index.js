const { initDatabase } = require('./init')
const { doMessagesExist } = require('./doMessagesExist')
const { writeFirstMessage } = require('./writeFirstMessage')
const { writeMessage } = require('./writeMessage')
const { updateLastReadDate } = require('./updateLastReadDate')


module.exports = {
  initDatabase,
  doMessagesExist,
  writeFirstMessage,
  writeMessage,
  updateLastReadDate,
}
