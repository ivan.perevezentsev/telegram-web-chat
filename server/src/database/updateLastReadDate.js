const firebase = require('firebase-admin')


exports.updateLastReadDate = function updateLastReadDate() {
  firebase.database().ref('/lastReadDate')
    .set(firebase.database.ServerValue.TIMESTAMP)
}
