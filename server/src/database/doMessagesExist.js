const R = require('ramda')
const admin = require('firebase-admin')

const { convertDbSnapshotToList } = require('../../../common/convertDbSnapshotToList')


exports.doMessagesExist = () =>
  admin.database().ref('/messages')
  .limitToFirst(1)
  .once('value')
  .then(isSnapshotNotEmpty)

const isSnapshotNotEmpty = R.pipe(
  convertDbSnapshotToList,
  R.isEmpty,
  R.not,
)
