const admin = require('firebase-admin')

const databaseURL = require('../../secrets/config').database.url
const serviceAccount = require('../../secrets/admin_credentials.json')


exports.initDatabase = function initDatabase() {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL,
  })
}
