const admin = require('firebase-admin')


// author - 'admin' | 'user'
exports.writeMessage = function writeMessage({ text, author }) {
  const dbEntry = {
    content: text,
    author,
    timestamp: admin.database.ServerValue.TIMESTAMP,
  }

  return admin.database().ref('/messages')
    .push(dbEntry)
}
