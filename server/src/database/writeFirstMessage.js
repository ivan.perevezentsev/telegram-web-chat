const admin = require('firebase-admin')


const firstTimeGreeting = 'Привет, юзер.'

const dbEntry = {
  content: firstTimeGreeting,
  author: 'admin',
  timestamp: admin.database.ServerValue.TIMESTAMP,
}

/// Writes first message as a transaction.
exports.writeFirstMessage = function writeFirstMessage() {
  return admin.database().ref('/messages')
    .transaction(messages => {
      if (!messages || Object.values(messages).length === 0) {
        return { 0: dbEntry }
      } else {
        return messages
      }
    })
}
