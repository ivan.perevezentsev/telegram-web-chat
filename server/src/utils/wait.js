exports.wait = millis => new Promise(
  resolve => setTimeout(resolve, millis)
)
