const process = require('process')


exports.catchUnhandledPromises = function catchUnhandledPromises() {
  process.on('unhandledRejection', error => {
    throw error
  })
}
