const { catchUnhandledPromises } = require('./catchUnhandledPromises')
const { wait } = require('./wait')


module.exports = {
  catchUnhandledPromises,
  wait,
}
