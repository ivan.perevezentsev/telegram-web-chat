const { catchUnhandledPromises } = require('./utils')
const { initDatabase } = require('./database')
const { initTelegramTransport } = require('./telegram')
const { initServer } = require('./server')
const { initWebsocketServer } = require('./websocketServer')
const { initFeatures } = require('./features')


async function main() {
  catchUnhandledPromises()
  initDatabase()
  const telegram = await initTelegramTransport()
  const server = initServer()
  const websocketServer = initWebsocketServer(server)
  initFeatures({ websocketServer, telegram })
}

main()
