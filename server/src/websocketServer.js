const socketIo = require('socket.io')


exports.initWebsocketServer = server =>
  socketIo(server)
