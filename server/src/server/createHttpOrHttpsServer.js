const https = require('https')
const http = require('http')
const fs = require('fs')
const process = require('process')


function createHttpsServer(app) {
  const options = {
    key: fs.readFileSync('./secrets/private.key'),
    cert: fs.readFileSync('./secrets/public.pem'),
  }

  return https.createServer(options, app)
}

function createHttpServer(app) {
  return http.createServer(app)
}

exports.createHttpOrHttpsServer = function createHttpOrHttpsServer(app) {
  const isProduction = process.env.NODE_ENV === 'production'

  if (isProduction) {
    return createHttpsServer(app)
  } else {
    return createHttpServer(app)
  }
}
