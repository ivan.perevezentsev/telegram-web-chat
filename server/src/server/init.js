const express = require('express')

const { createHttpOrHttpsServer } = require('./createHttpOrHttpsServer')
const { port } = require('../../secrets/config')


/** @returns {http.Server | https.Server} */
exports.initServer = function initServer() {
  const app = createApp()

  const server = createHttpOrHttpsServer(app)
  logOnceReady(server)
  logServerErrors(server)
  server.listen(port)

  return server
}

function createApp() {
  const app = express()
  setupRoutes(app)

  return app
}

function setupRoutes(app) {
  app.get('/', (req, res) => {
    res.end('This is Telegram web chat server.')
  })
}

function logOnceReady(server) {
  server.once('listening', () => {
    // eslint-disable-next-line no-console
    console.log(`Server is listening on port ${port}`)
  })
}

function logServerErrors(server) {
  server.on('clientError', error => {
    // eslint-disable-next-line no-console
    console.error('Client error occured:', error)
  })
}
