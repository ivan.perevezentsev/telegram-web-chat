exports.initTypingNotifications = function initTypingNotifications({
  websocketServer, telegram,
}) {
  const onConnection_ = onConnection.bind(null, telegram)

  websocketServer.on('connection', onConnection_)
}

function onConnection(telegram, socket) {
  const emitTypingEvent = () => {
    socket.emit('typing')
  }

  telegram.addListener('admin_typing', emitTypingEvent)

  // Unsubscribe when client disconnects
  socket.once('disconnect', () => {
    telegram.removeListener('admin_typing', emitTypingEvent)
  })
}
