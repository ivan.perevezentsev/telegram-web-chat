const { writeMessage } = require('../database')


exports.initMessageSending = function initMessageSending(telegram) {
  telegram.on('message_received', message => {
    if (message.author === 'admin') {
      writeMessage(message)
    }
  })
}
