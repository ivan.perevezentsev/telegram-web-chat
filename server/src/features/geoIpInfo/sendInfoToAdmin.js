exports.sendInfoToAdmin = async({ telegram, info }) => {
  const message = formatMessage(info)
  telegram.sendMessage(message)
}

const formatMessage = info => {
  const country = formatString(info.country_name)
  const city = formatString(info.city)
  const region = formatString(info.state_prov)
  const ip = formatString(info.ip)

  return `Client connected (${city}, ${region}, ${country}, ${ip})`
}

const formatString = string =>
  (!string || string === '')? 'Unknown': string
