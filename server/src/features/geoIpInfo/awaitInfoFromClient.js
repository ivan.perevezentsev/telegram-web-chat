const { awaitEvent } = require('../../../../common/awaitEvent')


exports.awaitInfoFromClient = async(socket) =>
  awaitEvent({
    emitter: socket,
    event: 'geoip info',
  })
