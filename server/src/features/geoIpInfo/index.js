const { initGeoIpInfo } = require('./initGeoIpInfo')

module.exports = {
  initGeoIpInfo,
}
