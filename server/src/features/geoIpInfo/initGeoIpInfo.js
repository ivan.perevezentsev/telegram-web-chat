const { awaitInfoFromClient } = require('./awaitInfoFromClient')
const { sendInfoToAdmin } = require('./sendInfoToAdmin')


exports.initGeoIpInfo = async function initGeoIpInfo({
  websocketServer, telegram,
}) {
  const onConnection_ = onConnection.bind(null, telegram)

  websocketServer.on('connection', onConnection_)
}

async function onConnection(telegram, socket) {
  const info = await awaitInfoFromClient(socket)

  logInfo(info)
  sendInfoToAdmin({ telegram, info })
}

function logInfo(info) {
  // eslint-disable-next-line no-console
  console.log('A client connected, geographic info:\n', info)
}
