const timeout = 1000 * 30 // ms


exports.initTelegramKeepAlive = function initTelegramKeepAlive(telegram) {
  const sendSignal_ = () => telegram.sendKeepAliveSignal()

  sendSignal_()
  setInterval(sendSignal_, timeout)
}
