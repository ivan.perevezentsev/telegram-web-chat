const { updateLastReadDate } = require('../database')


exports.initReadReceipts = telegram =>
  telegram.on('messages_read', updateLastReadDate)
