const writeMessageToDb = require('../database').writeMessage


exports.initMessageReceiving = function initMessageReceiving({
  websocketServer, telegram,
}) {
  const onConnection_ = onConnection.bind(null, telegram)

  websocketServer.on('connection', onConnection_)
}

async function onConnection(telegram, socket) {
  socket.on('user_sent_message', text => {
    writeMessageToDb({
      text,
      author: 'user',
    })

    telegram.sendMessage(text)
  })
}
