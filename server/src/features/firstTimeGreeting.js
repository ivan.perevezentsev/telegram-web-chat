const database = require('../database')
const { wait } = require('../utils')
const { timeout } = require('../../secrets/config').firstTimeGreeting


module.exports.initFirstTimeGreeting = function initFirstTimeGreeting(
  websocketServer
) {
  websocketServer.on('connection', onConnection)
}

async function onConnection(socket) {
  const messagesExist = await database.doMessagesExist()

  if (!messagesExist) {
    greetUser(socket)
  }
}

async function greetUser(socket) {
  socket.emit('typing')

  await wait(timeout)

  database.writeFirstMessage()
}
