const { initTelegramKeepAlive } = require('./telegramKeepAlive')
const { initMessageReceiving } = require('./messageReceiving')
const { initMessageSending } = require('./messageSending')
const { initFirstTimeGreeting } = require('./firstTimeGreeting')
const { initGeoIpInfo } = require('./geoIpInfo')
const { initReadReceipts } = require('./readReceipts')
const { initTypingNotifications } = require('./typingNotifications')


exports.initFeatures = function initFeatures({
  websocketServer, telegram,
}) {
  initTelegramKeepAlive(telegram)
  initMessageReceiving({ websocketServer, telegram })
  initMessageSending(telegram)
  initFirstTimeGreeting(websocketServer)
  initGeoIpInfo({ websocketServer, telegram })
  initReadReceipts(telegram)
  initTypingNotifications({ websocketServer, telegram })
}
