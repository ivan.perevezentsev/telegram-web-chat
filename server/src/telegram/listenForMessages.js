const config = require('../../secrets/config')


/// Implements `message_received` event
module.exports.listenForMessages =
  function listenForMessages(telegram, tdlClient)
{
  tdlClient.on('update', update => {
    if (isTargetUpdate(update)) {
      const message = unpackMessageFromUpdate(update)
      telegram.emit('message_received', message)
    }
  })
}

function isTargetUpdate(update) {
  const { message } = update

  return (
    update._ === 'updateNewMessage' &&
    message !== undefined &&
    message._ === 'message' &&
    message.content._ === 'messageText' &&
    message.content.text._ === 'formattedText' &&
    message.chat_id === config.telegram.chatId
  )
}

function unpackMessageFromUpdate(update) {
  const { message } = update

  return {
    text: message.content.text.text,
    author: message.is_outgoing? 'user': 'admin',
    date: message.date,
  }
}
