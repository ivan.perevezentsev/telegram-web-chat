/* eslint-env node */
const tdl = require('tdl')
const _ = require('lodash')
const EventEmitter = require('events')

const { sendMessage } = require('./sendMessage')
const { listChats } = require('./listChats')
const { sendKeepAliveSignal } = require('./sendKeepAliveSignal')
const { listenForMessages } = require('./listenForMessages')
const { listenForTyping } = require('./listenForTyping')
const { listenForChatRead } = require('./listenForChatRead')
const config = require('../../secrets/config')


const tdlibPath = 'third_party/tdlib/build/libtdjson'

exports.initTelegramTransport = async function createTelegramTransport() {
  const tdlClient = await createTdlClient()
  logClientErrors(tdlClient)

  const telegram = new EventEmitter()
  initTelegramMethods(telegram, tdlClient)
  initTelegramEvents(telegram, tdlClient)

  await workaroundTdlCrash(telegram)

  return telegram
}

function initTelegramMethods(telegram, tdlClient) {
  const methods = {
    sendMessage,
    listChats,
    sendKeepAliveSignal,
  }

  const boundMethod = method => method.bind(null, tdlClient)
  const boundMethods = _.mapValues(methods, boundMethod)

  Object.assign(telegram, boundMethods)
}

function initTelegramEvents(telegram, tdlClient) {
  [
    listenForMessages,
    listenForTyping,
    listenForChatRead,
  ]
    .forEach(method => method(telegram, tdlClient))
}

async function createTdlClient() {
  const clientOpts = _.merge(
    {},
    _.pick(config.telegram, ['apiId', 'apiHash']),
    {
      binaryPath: tdlibPath,
      loginDetails: {
        phoneNumber: config.telegram.phoneNumber,
      }
    },
  )
  const client = new tdl.Client(clientOpts)

  await client.connect()

  return client
}

function logClientErrors(client) {
  client
    // .on('update', err => {
    //   // eslint-disable-next-line no-console
    //   console.error('[Telegram] Got update:', JSON.stringify(err, null, 2))
    // })
    .on('error', err => {
      // eslint-disable-next-line no-console
      console.error('[Telegram] Got error:', JSON.stringify(err, null, 2))
    })
    .on('destroy', () => {
      // eslint-disable-next-line no-console
      console.log('[Telegram] Client destroyed.')
    })
}

// This function must be called right after initialization to avoid the crash
// which occurs after sign-in with the following message:
//
// ```
// terminate called after throwing an instance of 'Napi::Error'
//   what():  Chat not found
// Aborted (core dumped)
// ```
async function workaroundTdlCrash(telegram) {
  return telegram.listChats(1)
}
