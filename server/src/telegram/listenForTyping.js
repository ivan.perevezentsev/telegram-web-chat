const config = require('../../secrets/config')


exports.listenForTyping = function listenForTyping(telegram, tdlClient) {
  tdlClient.on('update', update => {
    if (isTypingAction(update)) {
      telegram.emit('admin_typing')
    }
  })
}

function isTypingAction(update) {
  const { action } = update

  return (
    update._ === 'updateUserChatAction' &&
    update.chat_id === config.telegram.chatId &&
    action !== undefined &&
    action._ === 'chatActionTyping'
  )
}
