exports.listChats = async function listChats(tdlClient, limit=100) {
  const chats = await tdlClient.invoke({
    _: 'getChats',
    offset_order: '9223372036854775807',
    offset_chat_id: 0,
    limit,
  })

  return chats
}
