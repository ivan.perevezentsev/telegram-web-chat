const config = require('../../secrets/config')


exports.sendKeepAliveSignal = function sendKeepAliveSignal(tdlClient) {
  tdlClient.invoke({
    _: 'openChat',
    chat_id: config.telegram.chatId,
  })
  tdlClient.invoke({
    _: 'setOption',
    name: 'online',
    value: {
      _: 'optionValueBoolean',
      value: true,
    },
  })
}
