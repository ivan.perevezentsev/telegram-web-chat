const config = require('../../secrets/config')


exports.listenForChatRead = function listenForChatRead(telegram, tdlClient) {
  tdlClient.on('update', update => {
    if (isTargetUpdate(update)) {
      telegram.emit('messages_read')
    }
  })
}

function isTargetUpdate(update) {
  return (
    update._ === 'updateChatReadOutbox' &&
    update.chat_id === config.telegram.chatId
  )
}
