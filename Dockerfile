FROM ubuntu:18.04

# TODO: mount the secrets folder

# TODO: make sure .gitignore files aren't copied

# Copy the source code
COPY . /app
WORKDIR /app

RUN apt-get update

RUN apt-get install -y git

RUN git submodule update --recursive --init

# Build TDLib
RUN apt-get install -y gcc g++ ccache openssl libssl-dev cmake gperf make git libreadline-dev zlib1g-dev
WORKDIR /app/server/third_party/tdlib
RUN mkdir build
WORKDIR build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN cmake --build .

RUN apt-get install -y curl gnupg

VOLUME ["/app/server/secrets"]

# Install Node.js 8
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get install -y yarn

# Install Node.js dependencies
WORKDIR /app/server
RUN yarn

# Expose HTTPS port
EXPOSE 443

# Launch the application
CMD ["yarn","start:prod"]
