/** Converts Firebase DB snapshot of the list to a plain JS array */
function convertDbSnapshotToList(snapshot) {
  const result = []

  snapshot.forEach(childSnapshot => {
    result.push({
      key: childSnapshot.key,
      data: childSnapshot.val(),
    })
  })

  return result
}

module.exports = {
  convertDbSnapshotToList,
}
