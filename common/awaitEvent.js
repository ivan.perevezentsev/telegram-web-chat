exports.awaitEvent = ({ emitter, event }) =>
  new Promise(resolve => {
    emitter.once(event, resolve)
  })
