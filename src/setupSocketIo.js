import io from 'socket.io-client'


/**
  Completes on `Socket` `connect` event.
  @returns {Socket}
*/
export const setupSocketIo = async() => new Promise(resolve => {
  const socket = io(CONFIG.apiAddress)

  socket.on('connect', () => resolve(socket))
})
