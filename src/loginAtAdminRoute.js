import * as firebaseui from 'firebaseui'
import * as firebase from 'firebase'


function setFormVisibility(visible: boolean) {
  const container = document.getElementById('firebaseui-auth-container')
  container.style.display = (visible? 'block': 'none')
}

function tryLogin(auth) {
  const ui = new firebaseui.auth.AuthUI(auth)

  setFormVisibility(true)

  ui.start('#firebaseui-auth-container', {
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      signInSuccess: (/*currentUser, credential, redirectUrl*/) => {
        setFormVisibility(false)
        history.pushState(undefined, undefined, '/')

        // `false` return value means we don't want to redirect automatically
        return false
      },
    },
  })
}

/// Show login form if we at `/admin` path
export function loginAtAdminRoute(auth) {
  const currentUrl = new URL(document.location.href)
  if (currentUrl.pathname === '/admin') {
    tryLogin(auth)
  }
}
