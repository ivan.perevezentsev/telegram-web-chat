import * as firebase from 'firebase'


export function initFirebaseApp() {
  // We provide the app name as a second argument to prevent Firebase usage via
  // the singleton, e.g. we don't want this:
  //
  // import * as firebase from 'firebase'
  // firebase.blabla()
  //
  // The reason is proper handling of code dependencies. We want the Firebase
  // app instance to be explicitly passed to the code that needs it.
  const appName = 'tg-chat'
  const app = firebase.initializeApp(CONFIG.firebaseConfig, appName)

  return app
}
