import React from 'react'
const R = require('ramda')

import { Message } from './Message'
import { scrollWindowToBottom } from '../scrollWindowToBottom'


const groupingInterval = CONFIG.messageGroupingInterval

/**
 * This component only contains visual messages logic, it doesn't know
 * anything about how data is retrieved.
 */
export class MessagesVisual extends React.Component {
  render() {
    const { messages } = this.props

    return messages.map((message, index) => {
      const messageElem = createMessageElement(this.props, index)

      if (shouldPrependDateToMessage(messages, index)) {
        return [
          createDateElement(message.key, message.data.timestamp),
          messageElem,
        ]
      } else {
        return messageElem
      }
    })
  }

  UNSAFE_componentWillUpdate() {
    this._saveScrollingOffset()
  }

  componentDidUpdate(prevProps) {
    this._updateScrolling({
      prevMessages: prevProps.messages,
      nextMessages: this.props.messages,
    })
  }

  _updateScrolling({ prevMessages, nextMessages }) {
    if (areNewMessagesReceived({ prevMessages, nextMessages })) {
      scrollWindowToBottom()
    } else {
      this._restorePreviousScrolling()
    }
  }

  _saveScrollingOffset() {
    this._previousBottomScroll = getBottomScroll()
  }

  _restorePreviousScrolling() {
    setBottomScroll(this._previousBottomScroll)
  }
}

function areNewMessagesReceived({ prevMessages, nextMessages }) {
  if (nextMessages.length === 0) {
    return false
  }
  if (prevMessages.length === 0 && nextMessages.length > 0) {
    return true
  }
  return R.last(nextMessages).key > R.last(prevMessages).key
}

// Scrolling helpers
const getScrollElem = () =>
  document.scrollingElement || document.documentElement
function getBottomScroll() {
  const el = getScrollElem()
  return el.scrollHeight - el.scrollTop
}
function setBottomScroll(x) {
  const el = getScrollElem()
  return el.scrollTop = el.scrollHeight - x
}

function createMessageElement(props, index) {
  const { messages, lastReadDate, deleteMessage, isAdmin } = props
  const { key, data } = messages[index]

  const isRead = isMessageRead(data.timestamp, lastReadDate)
  const isGroupedWithNext = isMessageGroupedWithNext(messages, index)
  const id = key

  return <Message key={'m' + key}
    {...{ id, data, isRead, isGroupedWithNext, deleteMessage, isAdmin }} />
}

const isMessageRead = (messageDate, lastReadDate) => messageDate <= lastReadDate

function isMessageGroupedWithNext(messages, index) {
  const isLast = index === messages.length - 1
  if (isLast) return false

  const authorsAreDifferent =
    messages[index].data.author !==
    messages[index + 1].data.author
  if (authorsAreDifferent) return false

  const getTime = index => messages[index].data.timestamp

  return getTime(index + 1) - getTime(index) <= groupingInterval
}

function shouldPrependDateToMessage(messages, index) {
  if (index === 0) {
    return true
  }

  const thisDate = new Date(messages[index].data.timestamp)
  removeTimeFromDate(thisDate)

  const prevDate = new Date(messages[index - 1].data.timestamp)
  removeTimeFromDate(prevDate)

  return !areDatesEqual(prevDate, thisDate)
}

const createDateElement = (key, timestamp) =>
  <div className='date-mark' key={'d' + key}>
    { formatDate(new Date(timestamp)) }
  </div>

function removeTimeFromDate(date) {
  date.setHours(0, 0, 0, 0)
}

const areDatesEqual = (a, b) => a.getTime() === b.getTime()

function formatDate(date) {
  date = new Date(date)
  removeTimeFromDate(date)

  const today = new Date()
  removeTimeFromDate(today)

  const yesterday = new Date(today)
  yesterday.setUTCDate(yesterday.getUTCDate() - 1)

  if (areDatesEqual(date, today)) {
    return 'сегодня'
  } else if (areDatesEqual(date, yesterday)) {
    return 'вчера'
  } else {
    const month = [
      'января',
      'февраля',
      'марта',
      'апреля',
      'мая',
      'июня',
      'июля',
      'августа',
      'сентября',
      'октября',
      'ноября',
      'декабря',
    ][date.getMonth()]
    return date.getDate() + ' ' + month
  }
}
