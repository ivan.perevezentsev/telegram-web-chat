import React from 'react'



export function DeleteButton (props) {
  const { messageId, deleteMessage } = props

  const className = 'admin_ui delete_message_button'
  const onClick = () => deleteMessage(messageId)

  return (
    <div {...{ className, onClick }}></div>
  )
}
