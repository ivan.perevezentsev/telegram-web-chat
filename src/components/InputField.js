import React from 'react'


export class InputField extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      text: '',
      isDisabled: false,
    }

    this._bindMethods()

    this._textareaRef = React.createRef()
  }

  render() {
    const placeholder = this.state.isDisabled ?
      'Подключение отсутствует' : 'Сообщение'

    let underlineClassName = 'input_field_underline'
    let buttonClassName = 'button send_button'
    let textareaValue
    if (this.state.isDisabled) {
      underlineClassName += ' input_field_underline_disabled'
      buttonClassName += ' send_button_disabled'
      textareaValue = ''
    } else {
      textareaValue = this.state.text
    }

    return (
      <form className='input_field_container'>
        <div className='input_field_centered'>
          <div className={ underlineClassName }>
            <textarea ref={ this._textareaRef } className='input_field'
              maxLength='4096' placeholder={ placeholder }
              onKeyDown={ this._handleKeyDown } onChange={ this._handleChange }
              value={ textareaValue } disabled={ this.state.isDisabled }>
            </textarea>
            <button type='button' className={ buttonClassName }
              onClick={ this._sendMessage }>
              <ButtonIcon />
            </button>
          </div>
        </div>
      </form>
    )
  }

  componentDidMount() {
    initAutoGrow(this._textareaRef.current)

    this._subscribeToConnectionEvents()
  }

  componentWillUnmount() {
    this._unsubscribeToConnectionEvents()
  }

  // Private methods

  _sendMessage() {
    this.setState((prevState, props) => {
      const message = prevState.text

      if (isValidMessage(message)) {
        const { socket } = props
        socket.emit('user_sent_message', message)

        // Manually trigger textarea resize
        this._textareaRef.current.dispatchEvent(new Event('keydown'))
      }

      return { text: '' }
    })
  }

  _handleChange(event) {
    this.setState({ text: event.target.value })
  }

  _handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter' && !event.shiftKey) {
      event.preventDefault()
      event.stopPropagation()
      this._sendMessage()
    }
  }

  // Connection indicator

  _subscribeToConnectionEvents() {
    const { socket } = this.props

    socket.on('connect', this._handleConnectionChange)
    socket.on('disconnect', this._handleConnectionChange)
  }

  _unsubscribeToConnectionEvents() {
    const { socket } = this.props

    socket.off(this._handleConnectionChange)
  }

  _handleConnectionChange() {
    const { socket } = this.props

    this.setState({
      isDisabled: socket.disconnected,
    })
  }

  // Other

  _bindMethods() {
    [
      '_handleKeyDown',
      '_handleChange',
      '_sendMessage',
      '_handleConnectionChange',
    ]
      .forEach(method => {
        this[method] = this[method].bind(this)
      })
  }
}

function initAutoGrow(elem) {
  const updateElemHeight_ = updateElemHeight.bind(null, elem)

  updateElemHeight_()

  elem.addEventListener('keydown', () => {
    window.setImmediate(updateElemHeight_)
  })
}

function updateElemHeight(elem) {
  const lineHeightPx = 21

  elem.style.height = '21px'
  elem.style.height = (elem.scrollHeight - lineHeightPx) + 'px'
}

const isValidMessage = message => /^\s*$/.test(message) === false

const ButtonIcon = () =>
  <svg fill='#444' width='40' height='40' version='1.1' viewBox='0 0 100 100'
    xmlns='http://www.w3.org/2000/svg'>
    <path d={'m82.883 18.859l-65.766 30.16 22.637 10.68v21.441l19.766-12.121 ' +
      '17.996 8.4883zm-7.9375 8.3164l-32.391 29.141-15.355-7.2422zm-30.941 ' +
      '46.371v-11.844l10.914 5.1484zm2.7227-15.266l30.816-27.723-3.707 40.512z'}
      />
  </svg>
