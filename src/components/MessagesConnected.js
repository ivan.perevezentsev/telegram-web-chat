import React from 'react'
const R = require('ramda')

import { MessagesVisual } from './MessagesVisual'
import { createMessageWindowReference, areAllMessagesLoaded, deleteMessage }
  from '../database'
import { listenLastReadDate } from '../database/listenLastReadDate';


// How many messages we want at start
const initialMessageNum = 25
// How many messages are loaded additionally per request
const messageIncrement = 100

/**
 * This component manages database reference and pipes its data
 * into MessagesVisual.
 */
export class MessagesConnected extends React.Component {
  constructor() {
    super()

    this.setState = this.setState.bind(this)
    this._emitFirstLoadEvent = this._emitFirstLoadEvent.bind(this)

    this.state = {
      messages: [],
      lastReadDate: 0,
    }
  }

  componentDidMount() {
    const { database } = this.props

    this._updateMessagesDbBinding(database, initialMessageNum)
    this._setupLastReadDateBinding(database)
  }

  // This is public method, should be called via React ref.
  loadMore() {
    const { database } = this.props

    const newMessageNum = this.state.messages.length + messageIncrement
    this._updateMessagesDbBinding(database, newMessageNum)
  }

  render() {
    const { onMessagesLoadedUpdate, database, isAdmin } = this.props
    this._onMessagesLoadedUpdate = onMessagesLoadedUpdate

    const { messages, lastReadDate } = this.state
    const deleteMessage_ = deleteMessage.bind(null, database)

    return <MessagesVisual {...{ messages, lastReadDate, isAdmin }}
            deleteMessage={deleteMessage_} />
  }

  /**
   * Creates a new DB binding with new message number.
   * @param messageNum {number} How many messages to load
   */
  async _updateMessagesDbBinding(database, messageNum) {
    if (this.messagesDbRef) {
      this.messagesDbRef.dispose()
    }

    this.messagesDbRef =
      await createMessageWindowReference(database, messageNum)

    const updateState = messages => this.setState({ messages })
    this.messagesDbRef.subscribe(updateState)
    this.messagesDbRef.subscribe(this._emitFirstLoadEvent)

    this._updateAllMessagesLoaded(database, messageNum)
  }

  _setupLastReadDateBinding(database) {
    const onValue = R.pipe(
      lastReadDate => ({ lastReadDate }),
      this.setState,
    )

    listenLastReadDate(database, onValue)
  }

  _emitFirstLoadEvent () {
    if (!this._messagesAreLoaded) {
      this._messagesAreLoaded = true

      const event = new Event('messages_loaded')
      window.dispatchEvent(event)
    }
  }

  async _updateAllMessagesLoaded (database, currentlyLoadedMessageNum) {
    const allMessagesLoaded =
      await areAllMessagesLoaded(database, currentlyLoadedMessageNum)

    if (this._onMessagesLoadedUpdate) {
      this._onMessagesLoadedUpdate(allMessagesLoaded)
    }
  }
}
