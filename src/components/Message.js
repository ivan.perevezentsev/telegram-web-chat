import React from 'react'

import { DeleteButton } from './DeleteButton'
import { Timestamp } from './Timestamp'


export function Message(props) {
  const { id, data, isRead, isGroupedWithNext, deleteMessage, isAdmin } = props
  const { content, timestamp } = data

  let className = `message message-${data.author}`
  if (isGroupedWithNext) {
    className += ' group-with-next'
  }

  const receiptIndicator = makeReceiptIndicator(isRead, data.author)

  return (
    <div {...{ id, className }}>
      { content }
      <Timestamp {...{ timestamp }} />
      { receiptIndicator }
      { isAdmin && <DeleteButton messageId={id} {...{ deleteMessage }} /> }
    </div>
  )
}

function makeReceiptIndicator(isRead, author) {
  if (author === 'admin') return null

  return (
    <span className='message-receipt-mark'>
      { isRead? '✓✓' : '✓' }
    </span>
  )
}
