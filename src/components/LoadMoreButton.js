import React from 'react'


export function LoadMoreButton(props) {
  const { onClick } = props

  return (
    <span className='load-more-button button' {...{ onClick }}>
      Load more
    </span>
  )
}
