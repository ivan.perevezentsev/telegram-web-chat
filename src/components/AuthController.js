import React from 'react'

import { isUserAdmin } from '../database'


export class AuthController extends React.Component {
  constructor(props) {
    super(props)

    this.state = { isAdmin: false }
  }

  render() {
    const renderFunc = this.props.render

    return renderFunc(this.state.isAdmin)
  }

  componentDidMount() {
    const { auth, database } = this.props

    listenAuthState(auth, database, isAdmin => {
      this.setState({ isAdmin })
    })
  }
}

function listenAuthState(auth, database, onComplete) {
  auth.onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      logUserInfo(user)

      isUserAdmin(database, user.uid)
        .then(onComplete)
    } else {
      // eslint-disable-next-line no-console
      console.log('User isn\'t signed in. Trying to sign in anonymously...')

      signInAnonymously(auth)
    }
  })
}

function signInAnonymously(auth) {
  if (auth.currentUser !== null) {
    return
  }

  auth.signInAnonymously()
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error('Couldn\'t sign in anonymously:', error)
    })
}

function logUserInfo(user) {
  /* eslint-disable no-console */
  if (user.isAnonymous) {
    console.log('Signed in anonymously.')
  } else {
    console.log('Signed in as user:', user.displayName, '(' + user.email + ')')
  }
  /* eslint-enable no-console */
}
