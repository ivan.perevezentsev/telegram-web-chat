import React from 'react'
import Rx from 'rxjs/Rx'

import { scrollWindowToBottom } from '../scrollWindowToBottom'


const typingEventLifetime = 7000

export class TypingNotification extends React.Component {
  constructor() {
    super()

    this.state = { isVisible: false }
  }

  render() {
    return this.state.isVisible && <Visual />
  }

  componentDidMount() {
    this._subscribeToEvents()
  }

  componentWillUnmount() {
    // TODO: unsubscribe from socket events
  }

  // Private methods

  _subscribeToEvents() {
    const { socket } = this.props

    const $typing = Rx.Observable.fromEvent(socket, 'typing')
    const $typingStopped = $typing.debounceTime(typingEventLifetime)

    // Side effects

    const setUiVisibility = isVisible => {
      this.setState({ isVisible })

      if (isVisible) scrollWindowToBottom()
    }

    $typing.subscribe(() => setUiVisibility(true))
    $typingStopped.subscribe(() => setUiVisibility(false))
  }
}

function Visual() {
  return (
    <div id='message-typing-notification' className='message message-admin'>
      <span className='typing-notification-dots'>
        <span>●</span>
        <span>●</span>
        <span>●</span>
      </span>
    </div>
  )
}
