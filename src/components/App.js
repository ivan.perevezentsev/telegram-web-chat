import React from 'react'

import { MessageArea } from './MessageArea'
import { InputField } from './InputField'
import { AuthController } from './AuthController'


export function App(props) {
  const { socket, database, auth } = props

  return (
    <div>
      <AuthController {...{ auth, database }} render={
        isAdmin => <MessageArea {...{ socket, database, isAdmin }} />
      } />
      <InputField {...{ socket }} />
    </div>
  )
}
