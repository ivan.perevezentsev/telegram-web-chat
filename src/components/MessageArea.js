import React from 'react'

import { scrollWindowToBottom } from '../scrollWindowToBottom'
import { LoadMoreButton } from './LoadMoreButton'
import { MessagesConnected } from './MessagesConnected'
import { TypingNotification } from './TypingNotification'


export class MessageArea extends React.Component {
  constructor() {
    super()

    this.state = {
      showLoadMore: false
    }

    this._messagesRef = React.createRef()

    this._loadMoreMessages = this._loadMoreMessages.bind(this)
    this._onMessagesLoadedUpdate = this._onMessagesLoadedUpdate.bind(this)
  }

  render() {
    const { socket, database, isAdmin } = this.props

    return (
      <div className='messages'>
        {
          this.state.showLoadMore ?
            <LoadMoreButton onClick={this._loadMoreMessages} /> :
            null
        }
        <MessagesConnected ref={this._messagesRef}
          onMessagesLoadedUpdate={this._onMessagesLoadedUpdate}
          {...{ database, isAdmin }} />
        <TypingNotification {...{ socket }} />
      </div>
    )
  }

  componentDidMount() {
    window.addEventListener('messages_loaded', () => {
      scrollWindowToBottom()
    })
  }

  // Private methods

  _loadMoreMessages() {
    this._messagesRef.current.loadMore()
  }

  _onMessagesLoadedUpdate(allMessagesLoaded) {
    const showLoadMore = !allMessagesLoaded
    this.setState({ showLoadMore })
  }
}
