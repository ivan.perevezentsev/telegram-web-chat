import React from 'react'

import { unixTimestampToString } from './unixTimestampToString'


export function Timestamp(props) {
  const { timestamp } = props

  return (
    <span className='timestamp'>
      { unixTimestampToString(timestamp) }
    </span>
  )
}
