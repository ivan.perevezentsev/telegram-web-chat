export function startOnPageLoad(main) {
  if (document.readyState === 'complete') {
    main()
  } else {
    window.addEventListener('load', main)
  }
}
