import { getGeoIpInfo } from './getGeoIpInfo'


export async function sendGeoIpInfo(socket) {
  const info = await getGeoIpInfo()
  socket.emit('geoip info', info)
}
