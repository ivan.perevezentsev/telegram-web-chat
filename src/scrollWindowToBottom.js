export function scrollWindowToBottom() {
  const elem = document.scrollingElement || document.documentElement
  elem.scrollTop = elem.scrollHeight
}
