export function hideLoadingIndicatorOnMessagesLoad () {
  window.addEventListener('messages_loaded', switchLoadingIndicatorToContent)
}

function switchLoadingIndicatorToContent () {
  setElementVisibility({
    selector: '.loading_spinner_container',
    visible: false,
  })
  setElementVisibility({
    selector: '.content',
    visible: true,
  })
}

function setElementVisibility({ selector, visible }) {
  const elem = document.querySelector(selector)
  elem.style.display = visible? '' : 'none'
}
