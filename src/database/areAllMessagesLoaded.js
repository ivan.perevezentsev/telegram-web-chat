import { loadLastMessages } from './private'


export async function areAllMessagesLoaded(database, currentlyLoadedMessageNum)
{
  const lastNPlusOneMessages =
    await loadLastMessages(database, currentlyLoadedMessageNum + 1)
  return lastNPlusOneMessages.length <= currentlyLoadedMessageNum
}
