import { convertDbSnapshotToList }
  from '../../../common/convertDbSnapshotToList'


export const loadLastMessages = (database, num) =>
  database.ref('messages')
    .limitToLast(num)
    .once('value')
    .then(convertDbSnapshotToList)
