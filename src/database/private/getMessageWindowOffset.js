import { loadLastMessages } from './loadLastMessages'


/**
 * Returns the key of the first message of the last `windowSize` messages.
 * That key may be passed to `Reference.startAt()`.
 * @param {number} windowSize How many last messages are needed
 * @return {Promise<string | null>} The key of the first message or `null`
 *   if there are no messages
 */
export const getMessageWindowOffset = async(database, windowSize) =>
  loadLastMessages(database, windowSize)
    .then(messages => {
      if (messages.length === 0) {
        return null
      } else {
        return messages[0].key
      }
    })
