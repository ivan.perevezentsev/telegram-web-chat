export function deleteMessage(database, messageId: string) {
  database.ref(`messages/${messageId}`)
    .remove()
}
