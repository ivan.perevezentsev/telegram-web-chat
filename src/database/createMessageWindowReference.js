import { convertDbSnapshotToList } from '../../common/convertDbSnapshotToList'
import { getMessageWindowOffset } from './private'


export async function createMessageWindowReference(database, messageNum) {
  const messagesRef = await createMessageWindowReferenceInternal(
    database, messageNum)

  return {
    subscribe(listener) {
      messagesRef.on('value', snapshot => {
        const values = convertDbSnapshotToList(snapshot)

        listener(values)
      })
    },

    dispose() {
      messagesRef.off()
    }
  }
}

async function createMessageWindowReferenceInternal(database, messageNum) {
  const offset = await getMessageWindowOffset(database, messageNum)
  const messagesRef = database.ref('messages')

  if (offset !== null) {
    return messagesRef.startAt(null, offset)
  }
  return messagesRef
}
