export function listenLastReadDate(database, listener) {
  database.ref('lastReadDate')
    .on('value', snapshot => {
      const value = snapshot.val()

      listener(value)
    })
}
