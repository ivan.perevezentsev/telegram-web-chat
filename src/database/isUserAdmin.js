export async function isUserAdmin(database, uid) {
  const snapshot = await database.ref(`/admins/${uid}`).once('value')
  const user = snapshot.val()

  return !!user
}
