import React from 'react'
import ReactDOM from 'react-dom'

import { App } from './components'


export function renderApp({ socket, database, auth }) {
  const container = document.querySelector('.content')

  ReactDOM.render(
    <App {...{ socket, database, auth }} />,
    container
  )
}
