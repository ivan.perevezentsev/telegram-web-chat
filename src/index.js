import { startOnPageLoad } from './startOnPageLoad'
import { hideLoadingIndicatorOnMessagesLoad } from './loadingIndicator'
import { initFirebaseApp } from './firebase'
import { renderApp } from './renderApp'
import { loginAtAdminRoute } from './loginAtAdminRoute'
import { setupSocketIo } from './setupSocketIo'
import { sendGeoIpInfo } from './sendGeoIpInfo'


async function main() {
  hideLoadingIndicatorOnMessagesLoad()

  const firebase = initFirebaseApp()
  const auth = firebase.auth()
  const database = firebase.database()

  loginAtAdminRoute(auth)

  const socket = await setupSocketIo()
  sendGeoIpInfo(socket)
  renderApp({ socket, database, auth })
}

startOnPageLoad(main)
