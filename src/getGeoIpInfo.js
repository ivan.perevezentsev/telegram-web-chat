function throwError(e) {
  let message = 'Error retrieving IP address information'
  if (e) {
    message += ':\n' + e.toString()
  }
  throw new Error(message)
}

/** @returns Example:
{
  "ip":"84.210.233.189",
  "continent_code":"EU",
  "continent_name":"Europe",
  "country_code2":"RU",
  "country_code3":"RUS",
  "country_name":"Russia",
  "country_capital":"Moscow",
  "state_prov":"Moscow",
  "district":"Central Administrative Okrug",
  "city":"Moscow",
  "zipcode":"",
  "latitude":"55.7522",
  "longitude":"37.6202",
  "is_eu":true,
  "calling_code":"+7",
  "country_tld":".ru",
  "languages":"ru,tt,xal,cau,ady,kv,ce,tyv,cv,udm,tut,mns,bua,myv,mdf,chm,ba,inh,tut,kbd,krc,av,sah,nog",
  "country_flag":"https://ipgeolocation.io/static/flags/ru_64.png",
  "isp":"OJSC \"VimpelCom\"",
  "connection_type":"",
  "organization":"JSC VimpelCom 8 Marta st.",
  "geoname_id":"524901",
  "currency":{
    "name":"Ruble",
    "code":"RUB"
  },
  "time_zone":{
    "name":"Europe/Moscow",
    "offset":3,
    "is_dst":false,
    "current_time":"2018-06-19 01:25:29.288+0300"
  }
}
*/
export async function getGeoIpInfo() {
  const url = `https://api.ipgeolocation.io/ipgeo?apiKey=${CONFIG.geoLocationApiKey}`

  let response
  try {
    response = await fetch(url)
  } catch (e) {
    throwError(e)
  }

  if (!response.ok) {
    throwError(`response code is ${response.status} ${response.statusText}`)
  }

  return response.json()
}
