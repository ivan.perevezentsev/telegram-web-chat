const randomWords = require('random-words')
const leftPad = require('left-pad')


const MESSAGE_NUM = 20000

// Interval between messages
const TIME_INTERVAL = 1000 * 60 // 1 minute in milliseconds


const randomBool = () => Math.random() >= 0.5

function generateRandomMessage() {
  return {
    content: randomWords({ min: 1, max: 20, join: ' ' }),
    direction: randomBool()? 'sent': 'received',
  }
}

function main() {
  const messages = {}

  for (let i = 0; i < MESSAGE_NUM; ++i) {
    const newMessage = generateRandomMessage()
    newMessage.timestamp = Date.now() - TIME_INTERVAL * (MESSAGE_NUM - 1 - i)

    const key = leftPad(i, MESSAGE_NUM.toString().length, '0')
    messages[key] = newMessage
  }

  // eslint-disable-next-line no-console
  console.log(JSON.stringify(messages, null, 2))
}
main()
