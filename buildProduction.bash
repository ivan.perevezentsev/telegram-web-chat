#!/bin/bash

# Delete old files
rm -rf build/production

mkdir -p build/production

# Copy common non-JS part
cp -r public/* build/production

# Delete the `generated` folder from debug build (TODO: this is a workaround)
rm -rf build/production/generated

# Run the build
export NODE_ENV=production
yarn run webpack --config=build_configs/webpack.production.config.js
