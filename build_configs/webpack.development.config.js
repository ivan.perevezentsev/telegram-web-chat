// TODO: add ESLint `nodejs` environment
const { createDefinePlugin, commonConfig, projectRoot } =
  require('./webpack.common.config')
const { developmentDefinitions } = require('./definitions')

const path = require('path')


const developmentDefinitions_ = {
  ...developmentDefinitions,
}

module.exports = {
  ...commonConfig,
  output: {
    ...commonConfig.output,
    path: path.resolve(projectRoot, 'public/generated'),
  },
  plugins: [
    createDefinePlugin(developmentDefinitions_),
  ],
  devtool: 'eval-source-map',
}
