// TODO: add ESLint `nodejs` environment
const MinifyPlugin = require('babel-minify-webpack-plugin')

const { createDefinePlugin, commonConfig, projectRoot } =
  require('./webpack.common.config')
const { productionDefinitions } = require('./definitions')

const path = require('path')


const minifyPlugin = new MinifyPlugin({
  keepFnName: false,
  keepClassName: false,
})

module.exports = {
  ...commonConfig,
  output: {
    ...commonConfig.output,
    path: path.resolve(projectRoot, 'build/production/generated'),
  },
  plugins: [
    createDefinePlugin(productionDefinitions),
    minifyPlugin,
  ],
}
