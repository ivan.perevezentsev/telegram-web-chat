const webpack = require('webpack')

const path = require('path')
const process = require('process')


const createDefinePlugin = (definitions) =>
  new webpack.DefinePlugin({
    CONFIG: JSON.stringify(definitions),
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
  })

const projectRoot = path.resolve(__dirname, '..')

const commonConfig = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env'],
          },
        },
      },
    ],
  },
}

module.exports = {
  createDefinePlugin,

  projectRoot,
  commonConfig,
}
