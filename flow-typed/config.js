declare var CONFIG: {
  firebaseConfig: {
    apiKey: string,
    authDomain: string,
    databaseURL: string,
  }
}
